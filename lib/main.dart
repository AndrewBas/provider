import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_app/title_list.dart';
import 'package:provider_app/title_provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => TitleProvider(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(

          primarySwatch: Colors.blue,

          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MyHomePage(title: 'Flutter Demo Home Page'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String appBarTitle = 'default';
  int _counter = 0;

  void _incrementCounter(String title) {
    setState(() {
    appBarTitle = title;
    });
  }

  @override
  Widget build(BuildContext context) {
    final titleData = Provider.of<TitleProvider>(context);
    final titles = titleData.titles;
    return Scaffold(
      appBar: AppBar(
        title: Text(appBarTitle),
      ),
      body: Center(
        child: Column(mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FlatButton(
              onPressed: () => _incrementCounter(titles[0]),
              child: Text(
                titles[0],
              ),
            ),
            FlatButton(
              onPressed: () => _incrementCounter(titles[1]),
              child: Text(
                titles[1],
              ),
            ),
            FlatButton(
              onPressed: () => _incrementCounter(titles[2]),
              child: Text(
                titles[2],
              ),
            ),
            FlatButton(
              onPressed: () => _incrementCounter(titles[3]),
              child: Text(
                titles[3],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
